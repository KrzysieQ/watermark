const Jimp = require('jimp');
const inquirer = require('inquirer');
const fs = require('fs');

const addTextWatermarkToImage = async function(inputFile, outputFile, text) {
  try {
    const image = await Jimp.read(inputFile);
    const font = await Jimp.loadFont(Jimp.FONT_SANS_32_BLACK);
    const textData = {
      text,
      alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
      alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE,
    }

    image.print(font, 0, 0, textData, image.getWidth(), image.getHeight());
    await image.quality(100).writeAsync(outputFile);
    console.log(`Watermark created successfully with "${text}" text.`);
  } catch (e) {
    console.log('Something went wrong... Try again');
    startApp();
  }
};

const addImageWatermarkToImage = async function(inputFile, outputFile, watermarkFile) {
  try {
    const image = await Jimp.read(inputFile);
    const watermark = await Jimp.read(watermarkFile);
    const x = image.getWidth() / 2 - watermark.getWidth() / 2;
    const y = image.getHeight() / 2 - watermark.getHeight() / 2;

    image.composite(watermark, x, y, {
      mode: Jimp.BLEND_SOURCE_OVER,
      opacitySource: 0.5,
    });
    await image.quality(100).writeAsync(outputFile);
    console.log(`Watermark created successfully with "${watermarkFile}" file.`);
  } catch (e) {
    console.log('Something went wrong... Try again');
    startApp();
  }
};

const prepareOutputFilename = (filename) => {
  const [name, ext] = filename.split('.');
  return `${name}-with-watermark.${ext}`;
}

const startApp = async () => {
  const answer = await inquirer.prompt([{
    name: 'start',
    message: 'Hi! Welcome to "Watermark manager". Copy your image files to `/img` folder. Then you\'ll be able to use them in the app. Are you ready?',
    type: 'confirm'
  }]);
  if(!answer.start) process.exit();

  const options = await inquirer.prompt([{
    name: 'inputImage',
    type: 'input',
    message: 'What file do you want to mark?',
    default: 'test.jpg',
  },{
    name: 'watermarkType',
    type: 'list',
    choices: ['Text watermark', 'Image watermakr'],
  }]);
  if(options.watermarkType === 'Text watermark') {
    const text = await inquirer.prompt([{
      name: 'value',
      type: 'input',
      message: 'Type your watermark text:',
    }]);
    options.watermarkText = text.value;
    const exists = fs.existsSync(`./img/${options.inputImage}`);
    if(!exists) return console.error(`Error: input file not exists... Try again.`);
    addTextWatermarkToImage(`./img/${options.inputImage}`, `./img/${prepareOutputFilename(options.inputImage)}`, options.watermarkText);
  } else {
    const image = await inquirer.prompt([{
      name: 'filename',
      type: 'input',
      message: 'Type your watermark name:',
      default: 'logo.png',
    }]);
    options.watermarkImage = image.filename;
    const exists = {
      'input': fs.existsSync(`./img/${options.inputImage}`),
      'watermark': fs.existsSync(`./img/${options.watermarkImage}`)
    };
    for(let prop in exists) {
      if(!exists[prop]) return console.error(`Error: ${prop} file not exists... Try again.`);
    }
    addImageWatermarkToImage(`./img/${options.inputImage}`, `./img/${prepareOutputFilename(options.inputImage)}`, `./img/${options.watermarkImage}`);
  }
}

startApp();